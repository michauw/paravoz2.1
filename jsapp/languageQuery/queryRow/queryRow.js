corpus.directive('queryRow', ['$http', 'queryKeeper', 'ngDialog', function($http, queryKeeper, ngDialog) {
    return {
        templateUrl: 'jsapp/languageQuery/queryRow/queryRow.html',
        restrict: 'E',
        scope: {
            showTokensInBetween: '=',
            index: '=',
            language: '=',
        },
        controller: function($scope, $rootScope) {

			var popUpOpened = false;
			var showWarning = true;
			$scope.showAspect = $scope.language.name == 'pol';
            console.log ('lang:', $scope.language.name);
			$scope.aspect = {
				values: [{name: 'asp. value', val: ''}, {name: 'perf_patner', val: 'p'}, {name: 'imp_patner', val: 'i'}, {name: 'perf_tantum', val: 'p_tn'}, {name: 'imp_tantum', val: 'i_tn'}, {name: 'biaspect', val: 'bi'}],
				determ: [{name: 'asp. determiner', val: ''}, {name: 'simplex', val: 'si'}, {name: 'prefix', val: 'pr'}, {name: 'sufix', val: 'su'}, {name: 'suplet.', val: 'sp'}, {name: 'gramat.', val: 'gr'}]
			}
            $scope.queryRow = queryKeeper.get($scope.language, $scope.index);
            $scope.$watchCollection('queryRow', function(newValue, oldValue) {
				// if (!showWarning)
					// queryKeeper.setCqpChanged ('');             
				if (queryKeeper.getCqpChanged($scope.language) && !popUpOpened && showWarning)
				{
					popUpOpened = true;
					ngDialog.openConfirm({
						template: 'jsapp/languageQuery/queryRow/modal.html',
						className: 'ngdialog-theme-default'
					}).then(function (hide) {
						// queryKeeper.setCqpChanged ('');
						queryKeeper.set($scope.language, $scope.index, newValue);
						popUpOpened = false;
						showWarning = !hide;
					}, function (hide) {
						queryKeeper.set($scope.language, $scope.index, oldValue);
						popUpOpened = false;
						showWarning = !hide;
					});
				}
				else
				{
                    console.log ('in watch, old:', oldValue);
                    console.log ('in watch, new:', newValue);
					queryKeeper.set($scope.language, $scope.index, newValue);
				}
            });
			$scope.getFields = function (){
				return queryKeeper.get($scope.language, $scope.index);
			}
			$scope.autocmp = function(ids, atype) {
				$scope.atype = atype;
				getSuggestion (ids, atype, $scope.language, $scope);
				// console.log ('auto:', ids, atype);
			}
			$scope.$on ('qR change', function () {
				$scope.queryRow = queryKeeper.get ($scope.language, $scope.index);
			});
            $scope.stchanged = function (attr, st) {
                console.log ('changed to:', st);
                $scope.queryRow[attr] = st;
                console.log ('queryRow:', $scope.queryRow);
            };
            $scope.getValues = function (query, attr) {
                var cqlQuery = '"' + query + '.*"';
                params = {query : cqlQuery, attribute: attr, language: $scope.language.name, method: 'positional'};
                console.log ('params:', params);
                return $http.get('backend/autocomplete.php', {
                    params: params
                    })
                    .then(function(data) {
                        var d = data.data;
                        console.log ('d:', data);
                        return d;
                    });
            }
        }
    };
}]);
