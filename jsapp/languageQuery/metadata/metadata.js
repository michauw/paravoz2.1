corpus.directive('metadata', ['$http', 'metaKeeper', 'gettextCatalog', function($http, metaKeeper, gettextcatalog) {
    return {
        templateUrl: 'jsapp/languageQuery/metadata/metadata.html',
        restrict: 'E',
        scope: {
			index: '=',
			place: '=',
            lang: '='
        },
        controller: function($scope, gettextCatalog) {
            $scope.searchText = '';
            $scope.meta = metaKeeper.get ($scope.index, $scope.place);
			//$scope.tip = gettextCatalog.getString('show') + ' <b> ' + $scope.meta.hint + '</b> ' + gettextCatalog.getString('in results');
            $scope.$watchCollection('meta', function(newValue, oldValue) {
                metaKeeper.set ($scope.index, $scope.place, newValue);
				if (newValue.inResults != oldValue.inResults)
				{
					console.log($scope.index);
					console.log($scope.place);
				}
            });

			$scope.getTip = function () {
				return gettextCatalog.getString('show') + ' <b> ' + $scope.meta.hint + '</b> ' + gettextCatalog.getString('in results');
			}
            $scope.getValues = function (query) {
                console.log ('meta:', $scope.meta);
                console.log ('lang:', $scope.lang);
                params = {name: $scope.meta.name, language: $scope.lang.name, query: query};
                console.log ('params:', params);
                return $http.get('backend/autocomplete_meta.php', {
                    params: params
                    })
                    .then(function(data) {
                        console.log ('data:', data);
                        var d = data.data;
                        return d;
                    });
            }

        }
    };
}]);
