corpus.directive ('appSettings', ['$cookies', 'settingsKeeper', 'queryKeeper', 'modeKeeper', function ($cookies, settingsKeeper, queryKeeper, modeKeeper) {
    return {
        templateUrl: 'jsapp/settings/settings.html',
        restrict: 'E',
        scope: {
            languages: '=',
            settings: '=',
			isOpen: '=',
        },
        controller: function($scope) {
            $scope.mode = modeKeeper.get ('name');

            $scope.toggleLanguage = function (lang) {
                $scope.mode = modeKeeper.get ('name');

                if ($scope.mode == 'mono') {
                    for (var i = 0; i < $scope.languages.length; ++i)
                    {

                        if ($scope.languages[i] != lang)
                            $scope.languages[i].use = false;
                    }
                    var curIndex = ['au', 'fr', 'uk'].indexOf (lang.name.slice (9)) + 1;

                    modeKeeper.set ('currentCorpusIndex', curIndex);
                    modeKeeper.set ('lastMonoIndex', curIndex);


                }
                queryKeeper.setLanguages ($scope.languages, $scope.mode, true);
            }
            $scope.close = function () {
                $scope.isOpen = false;
            }
			$scope.restore = function () {
				$scope.settings = angular.copy (settingsKeeper.getDefault ());
				queryKeeper.setLanguages (angular.copy (queryKeeper.getDefaultLanguages ()));
			}

        }
    };
}]);
