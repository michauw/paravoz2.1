import re, sys, subprocess, json, os
from math import log, sqrt
from collections import OrderedDict as ordd

if __name__ == '__main__':

    import sys
    sys.stderr.write ('params:\n')
    sys.stderr.write (sys.argv[1] + '\n')
    sys.stderr.write ('\nsettings:\n')
    sys.stderr.write (sys.argv[2] + '\n\n')

    freqDir = 'resources'
    params = json.loads (sys.argv[1], encoding = 'utf8')
    if 'sort' in params:
        sort = [el[0] for el in AMs].index (params['sort']) + 1
    else:
        sort = 1
    settings = json.loads (sys.argv[2])
    #cwbdir, registry, corpus_name, query, primQuery, primLang = sys.argv[1:-2]
    #query = query.replace (r'\"', '"')
    #primQuery = primQuery.replace (r'\"', '"')
    #langs = sys.argv[-2].replace (r'\"', '"')
    #settings = json.loads (sys.argv[-1], encoding = 'utf8')
    if not params['primquery']:
        params['primquery'] = '[]'
    query = params['primquery'] + params['secondquery']
    countBy = settings['countBy']
    threshold = settings['threshold']
    size = settings['size']
    if '%c' in params['primquery']:
        cs = True
    else:
        cs = False

    command = [os.path.join (params['cwbdir'], 'cqpcl'), '-r', params['registry'], '%s; set Context %d words; show -cpos; show +lemma; %s;' % (params['corpusname'], size - 1, query)]

    # sys.stderr.write (subprocess.list2cmdline (command) + '\n\n')
    # sys.stderr.write ('query: %s\n\n' % query)
    proc = subprocess.Popen (command, stdout = subprocess.PIPE)
    results = proc.communicate ()[0]
    # with open ('/var/www/html/Birmingham/resources/ngram-out.txt', 'w') as fout:
        # fout.write (results)
    #print results.splitlines ()[:2]

    ngrams = {}
    node = u''
    for line in results.decode ('utf8').splitlines ():
        # print 'line:', line
        words = [el.rsplit ('/', 1)[countBy == 'lemma' and 1 or 0] for el in line.strip ().split ()]
        if countBy == 'word':
            words[size - 1] = words[size - 1][1:]
        else:
            words[size - 1] = words[size - 1][:-1]
        if not cs:
            words = [w.lower () for w in words]
        words = tuple (words)
        for ng in [words[i:i+size] for i in range (len (words) - size + 1)]:
            if ng in ngrams:
                ngrams[ng] += 1
            else:
                ngrams[ng] = 1
    ngrams = sorted ([(ngram, freq) for ngram, freq in ngrams.items () if freq > threshold], key = lambda x: x[1], reverse = True)

    print query
    print len (ngrams)
    for ind, ngram in enumerate (ngrams):
        words, freq = ngram
        row = ' '.join (words) + '\t' + str (freq)
        print row
