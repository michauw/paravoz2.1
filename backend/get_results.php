<?php
    include ('../settings/init.php');
    $params = json_decode (file_get_contents ('php://input'), true);
	$init_mode = $params['init'];
    $langs = Array ();
    for ($i = 0; $i < count ($params['langs']); $i++)
        $langs[$params['langs'][$i]['name']] = $params['langs'][$i];
    $primary = $params['primlang']['name'];
	$meta = implode (', ', $params['meta']);
	if (!empty ($meta))
        $meta = 'set PrintStructures "' . $meta . '";';
    $corpus = strtoupper ($langs[$primary]['corpus']) . ';';
    $context = 'set Context Align_DE_PL;';
    $mode = '';
    $queries = $params['queries'];
    $count = 'size Last;';
    $query = '';
    $show = 'show +lemma +tag +atag +superlemma';
	$interval = 500;
    foreach ($queries as $name => $value) {     // it assumes that primary language is first
        if ($name == $primary)
            $query .= $value;
        else {
			if (!empty ($value)) {
				$query .= ': ' . strtoupper ($langs[$name]['corpus']) . ' ' . $value;
			}
            $show .= ' +' . strtolower ($langs[$name]['corpus']);
        }
    }
	$show .= ';';
    $query .= '; ';
	$query_initial = "A=$query";
    $command_initial = "$CWBDIR" . "cqpcl -r $REGISTRY '$mode $context $corpus $show $query_initial $count'";
    file_put_contents ('comminit.out', $command_initial);
	exec ($command_initial, $number_results);
	if ($init_mode) {
		echo $query . "\n";
		$resNumb = intval ($number_results[0]);
		echo $number_results[0];
	}
	else {
		$resNumb = intval ($number_results[0]);
		if ($resNumb <= $interval) {
			$command = "$CWBDIR" . "cqpcl -r $REGISTRY '$mode $context $corpus $show $meta $query'";
            file_put_contents ('comm1.out', $command);
			exec ($command, $results);
			$results = implode ("\n", $results);
            file_put_contents ('results.out', $results);
			echo $results;
		}
		else {
			for ($i = 0; $i <= $resNumb; $i += $interval) {
				$j = $i + $interval;
				$cat = "cat A $i $j;";
				$command = "$CWBDIR" . "cqpcl -r $REGISTRY '$mode $context $corpus $show $meta $query_initial $cat'";
                file_put_contents ('comm2.out', $command);
				exec ($command, $results);
				$results = implode ("\n", $results);
				echo $results;
				echo "\n";
			}
		}
	}
?>
