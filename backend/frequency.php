<?php
include ('../settings/init.php');
$params = json_decode (file_get_contents ('php://input'), true);
$primlang = $params['primlang'];
$queries = $params['queries'];
$langs = $params['langs'];
$countby = $params['countby'];
$init = $params['init'];
$actquery = $CORPUSNAME[$primlang] . '; F='. $queries[$primlang];
foreach ($langs as $l) {
	if (($l != $primlang) && $queries[$l])
		$actquery .= ': ' . $CORPUSNAME[$l] . ' ' . $queries[$l];
}
if (isset($_POST['metaToShow'])) {
	$_POST['metaToShow'] = json_decode ($_POST['metaToShow']);
}

$command = "python frequency.py $CWBDIR" . "cqpcl -r $REGISTRY" . " '$actquery; count F by $countby;'";

exec ($command, $out);

$actquery = substr ($actquery, strpos ($actquery, 'F=') + 2);
echo $actquery . "\n";
echo count ($out) . "\n";
echo implode ("\n", $out);
?>
