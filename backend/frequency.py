import re, sys, subprocess, json

if __name__ == '__main__':
    command = sys.argv[1:]

    proc = subprocess.Popen (command, stdout = subprocess.PIPE)
    results = proc.communicate ()[0]

    lpos = command[3].find ('F=') + 2
    rpos = command[3].find (';', lpos)
    match = command[3][lpos:rpos]
    query = command[3][lpos - 2: command[3].find ('; count') + 1]

    output = []
    for ind, line in enumerate (results.splitlines ()):
        els = line.strip ().split ()
        freq, word = els[:2]
        print '%s\t%s' % (word, freq)
