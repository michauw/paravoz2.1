import sys
import os

path = sys.argv[1]
try:
    query = sys.argv[2]
except IndexError:
    query = '';
out = []
with open (path) as fin:
    lines = sorted (fin.read ().split ('\x00'))
for l in lines:
    if (l.strip () and l.lower ().startswith (query)):
        print l.strip ()

